FROM openjdk:11-jdk
MAINTAINER Yan Santos

COPY target/*.jar /app/app.jar
COPY migrate.sh /app/migrate.sh

WORKDIR /app

EXPOSE 3000
ENTRYPOINT ["java", "-jar", "app.jar"]