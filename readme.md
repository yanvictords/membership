# USER-TEAM Relationships <br>

Author: Yan Victor dos Santos<br>
Email: yanvictords@gmail.com
<br><br>
Considering an existing service that provides management of user and team data, this module is
responsible for managing the relationship (membership) between these domains, adding to the user a role inside of team. 

## API domains:<br>
- Membership: is the user-team relationship, where the user can be part of one or more teams
- Role: is the user responsability within a team

## API usecases:<br>

- Given a team and user id's, when searching for the membership informations, we can se all the infors (including the membership role)
- Given a role, when searching for the memberships of this role, returns a list of memberships
- Given a list of all domain roles, when searching for it, then returns a list of these role names
- Given a new membership, when providing a role, creates a new role membership
- Given a role list of size n, when inserts a new one, the list becomes the size n+1
  <br>

## Architecture
The project was built using the Clean Architecture (Robert Cecil Martin) concepts, thinking about the scalability and maintenance. It's possible to integrate with some user-team service, just providing their identifications, or improve our domain core to manage the User and Team data too.

![alt text](/assets/clean.png?raw=true "Clean Architecture")

## Technologies:

- Java 11
- Maven
- Spring Boot
- Docker
- docker-compose
- MySQL (port 3306)
- Flayway for migrations
- JUnit for unit tests
- Swagger for API doc

SO Linux: Ubuntu 20.04.1 LTS.

<br>

## To build and run this project, you will need:

- Maven (~v3.6)
- Docker (~v20.10.7)
- docker-compose (~v1.25.0)

<br>

## Run:

At the main folder of the project:

- Build

```
1) mvn clean install
```

<br>

- Run the MySQL database and the App server

```
2) docker-compose up --build
```

<br>

- Run the migrations!

```
3) bash migrate.sh
```

GREAT! now you can see the API doc at the follow url:

- servidor=localhost<br><br>
```
http://${servidor}:3000/swagger-ui.html
```


## Next steps:

- Membership list by role API: insert pagination to avoid heavy query in database
- Bring to this module the manager responsability of User and Team domains, providing API's for CRUD
