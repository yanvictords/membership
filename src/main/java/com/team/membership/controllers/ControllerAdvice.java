package com.team.membership.controllers;

import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

import com.team.membership.controllers.contracts.ErrorContract;
import com.team.membership.controllers.converters.ErrorContractConverter;
import com.team.membership.exceptions.BaseException;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;

@org.springframework.web.bind.annotation.ControllerAdvice
@RequiredArgsConstructor
public class ControllerAdvice {

  @ExceptionHandler(BaseException.class)
  public ResponseEntity<List<ErrorContract>> handleBaseException(final BaseException e) {
    final var errorBody = singletonList(ErrorContractConverter.convert(e));
    return ResponseEntity.status(e.getStatus()).body(errorBody);
  }

  @ExceptionHandler(BindException.class)
  public ResponseEntity<List<ErrorContract>> handleMethodArgumentNotValidException(
      final BindException e) {
    final var errorBody =
        e.getBindingResult().getFieldErrors().stream()
            .map(
                it ->
                    new ErrorContract(
                        format("The field %s %s", it.getField(), it.getDefaultMessage())))
            .collect(toList());

    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorBody);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<List<ErrorContract>> handleUncaughtException(final Exception e) {
    final var errorBody = singletonList(new ErrorContract("Unexpected Error. :("));
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorBody);
  }
}
