package com.team.membership.controllers;

import static com.team.membership.controllers.converters.TeamContractConverter.convert;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import com.team.membership.controllers.contracts.ErrorContract;
import com.team.membership.controllers.contracts.RoleContract;
import com.team.membership.controllers.contracts.TeamContract;
import com.team.membership.controllers.converters.TeamContractConverter;
import com.team.membership.domains.Membership;
import com.team.membership.usecases.MembershipUsecase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/teams", produces = "application/json;charset=UTF-8")
public class MembershipController {

  private final MembershipUsecase membershipUsecase;

  @Operation(summary = "Get membership by team uuid and user uuid")
  @GetMapping("/{team_uuid}/users/{user_uuid}")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Membership",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = TeamContract.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class)))),
        @ApiResponse(
            responseCode = "404",
            description = "Membership not found",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class)))),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class))))
      })
  public ResponseEntity<TeamContract> findMembership(
      @PathVariable("team_uuid") final String teamUuid,
      @PathVariable("user_uuid") final String userUuid) {
    final var membership = membershipUsecase.findMembership(teamUuid, userUuid);
    return ResponseEntity.status(OK).body(convert(membership));
  }

  @Operation(summary = "Get all memberships by role")
  @GetMapping("/users/roles/{role_name}")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Membership",
            content = {
              @Content(
                  mediaType = "application/json",
                  array = @ArraySchema(schema = @Schema(implementation = TeamContract.class)))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class)))),
        @ApiResponse(
            responseCode = "404",
            description = "Role not found",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class)))),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class))))
      })
  public ResponseEntity<List<TeamContract>> findByRole(
      @PathVariable("role_name") final String roleName) {
    final var teams = membershipUsecase.findMembershipByRole(roleName);
    final var contracts = TeamContractConverter.convert(teams);
    return ResponseEntity.status(OK).body(contracts);
  }

  @Operation(summary = "Register new membership")
  @PostMapping("/{team_uuid}/users/{user_uuid}")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "Membership registered.",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = TeamContract.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class)))),
        @ApiResponse(
            responseCode = "404",
            description = "Role not found",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class)))),
        @ApiResponse(
            responseCode = "409",
            description = "Membership already registered.",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class)))),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class))))
      })
  public ResponseEntity<TeamContract> saveMemberShip(
      @PathVariable("team_uuid") final String teamUuid,
      @PathVariable("user_uuid") final String userUuid,
      @Valid @RequestBody final RoleContract request) {
    final var team = new Membership(teamUuid, userUuid);

    membershipUsecase.save(team, request.getRole());

    return ResponseEntity.status(CREATED).body(convert(team));
  }
}
