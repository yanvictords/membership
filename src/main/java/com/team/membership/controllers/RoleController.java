package com.team.membership.controllers;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import com.team.membership.controllers.contracts.ErrorContract;
import com.team.membership.controllers.contracts.RoleContract;
import com.team.membership.controllers.contracts.TeamContract;
import com.team.membership.domains.Role;
import com.team.membership.usecases.RoleUsecase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/roles", produces = "application/json;charset=UTF-8")
public class RoleController {

  private final RoleUsecase roleUsecase;

  @Operation(summary = "Get all registered roles")
  @GetMapping("")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Get All Roles",
            content = {
              @Content(
                  mediaType = "application/json",
                  array = @ArraySchema(schema = @Schema(implementation = String.class)))
            }),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class))))
      })
  public ResponseEntity<List<String>> findAll() {
    final var roles = roleUsecase.findAll().stream().map(Role::getName).collect(toList());
    return ResponseEntity.status(OK).body(roles);
  }

  @Operation(summary = "Register new role")
  @PostMapping("")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "Role registered.",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = TeamContract.class))
            }),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class)))),
        @ApiResponse(
            responseCode = "409",
            description = "Role already registered.",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class)))),
        @ApiResponse(
            responseCode = "500",
            description = "Internal server error",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = ErrorContract.class))))
      })
  public ResponseEntity<RoleContract> saveMembership(
      @Valid @RequestBody final RoleContract request) {
    roleUsecase.save(request.getRole());
    return ResponseEntity.status(CREATED).body(request);
  }
}
