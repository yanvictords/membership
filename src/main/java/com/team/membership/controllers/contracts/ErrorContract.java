package com.team.membership.controllers.contracts;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorContract {

  private final String error;
}
