package com.team.membership.controllers.contracts;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TeamContract {
  @NotEmpty(message = "cannot be empty")
  @Schema(name = "uuid", description = "the team uuid")
  private String uuid;

  @NotEmpty(message = "cannot be empty")
  @Schema(name = "users", description = "the membership users")
  private List<UserContract> users;
}
