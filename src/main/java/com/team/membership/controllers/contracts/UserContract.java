package com.team.membership.controllers.contracts;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotEmpty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserContract {
  @NotEmpty(message = "cannot be empty")
  @Schema(name = "uuid", description = "the user uuid")
  private String uuid;

  @NotEmpty(message = "cannot be empty")
  @Schema(name = "role", description = "the user role")
  private String role;
}
