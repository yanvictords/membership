package com.team.membership.controllers.converters;

import com.team.membership.controllers.contracts.ErrorContract;
import com.team.membership.exceptions.BaseException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorContractConverter {

  public static ErrorContract convert(final BaseException exception) {
    return new ErrorContract(exception.getMessage());
  }
}
