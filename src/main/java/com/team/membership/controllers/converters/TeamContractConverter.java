package com.team.membership.controllers.converters;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import com.team.membership.controllers.contracts.TeamContract;
import com.team.membership.domains.Membership;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TeamContractConverter {

  public static TeamContract convert(final Membership membership) {
    final var user = UserContractConverter.convert(membership);
    return new TeamContract(membership.getTeamUuid(), singletonList(user));
  }

  public static List<TeamContract> convert(final List<Membership> memberships) {
    return memberships.stream().collect(groupingBy(Membership::getTeamUuid)).entrySet().stream()
        .map(
            team -> {
              final var users =
                  team.getValue().stream().map(UserContractConverter::convert).collect(toList());
              return new TeamContract(team.getKey(), users);
            })
        .collect(toList());
  }
}
