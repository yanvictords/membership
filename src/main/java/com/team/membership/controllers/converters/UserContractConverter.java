package com.team.membership.controllers.converters;

import com.team.membership.controllers.contracts.UserContract;
import com.team.membership.domains.Membership;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserContractConverter {

  public static UserContract convert(final Membership membership) {
    return new UserContract(membership.getUserUuid(), membership.getRoleName());
  }
}
