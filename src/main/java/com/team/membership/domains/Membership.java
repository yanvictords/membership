package com.team.membership.domains;

import static org.apache.logging.log4j.util.Strings.EMPTY;

import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class Membership {
  private final String teamUuid;
  private final String userUuid;
  @Setter private Role role;

  public Membership(final String teamUuid, final String userUuid) {
    this.teamUuid = teamUuid;
    this.userUuid = userUuid;
  }

  public String getRoleName() {
    return Optional.ofNullable(role).map(Role::getName).orElse(EMPTY);
  }
}
