package com.team.membership.domains;

import java.math.BigInteger;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Role {
  private final BigInteger id;
  private final String name;
}
