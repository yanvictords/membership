package com.team.membership.exceptions;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class BadRequestException extends BaseException {

  public BadRequestException(final String message) {
    super(BAD_REQUEST, message);
  }
}
