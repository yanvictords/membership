package com.team.membership.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BaseException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final HttpStatus status;
  private final String message;

  public BaseException(final HttpStatus status, final String message) {
    this.status = status;
    this.message = message;
  }
}
