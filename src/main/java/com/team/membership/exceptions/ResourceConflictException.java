package com.team.membership.exceptions;

import static org.springframework.http.HttpStatus.CONFLICT;

public class ResourceConflictException extends BaseException {

  public ResourceConflictException(final String message) {
    super(CONFLICT, message);
  }
}
