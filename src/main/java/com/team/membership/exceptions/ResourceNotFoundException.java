package com.team.membership.exceptions;

import static org.springframework.http.HttpStatus.NOT_FOUND;

public class ResourceNotFoundException extends BaseException {

  public ResourceNotFoundException(final String message) {
    super(NOT_FOUND, message);
  }
}
