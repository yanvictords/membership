package com.team.membership.gateways;

import com.team.membership.gateways.mysql.entities.MembershipEntity;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface MembershipGateway {
  Optional<MembershipEntity> findByTeamAndUser(String teamUuid, String userUuid);

  List<MembershipEntity> findByRoleId(BigInteger roleId);

  void save(MembershipEntity team);
}
