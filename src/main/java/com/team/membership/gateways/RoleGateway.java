package com.team.membership.gateways;

import com.team.membership.gateways.mysql.entities.RoleEntity;
import java.util.List;
import java.util.Optional;

public interface RoleGateway {
  List<RoleEntity> findAll();

  Optional<RoleEntity> findByName(String name);

  void save(RoleEntity role);
}
