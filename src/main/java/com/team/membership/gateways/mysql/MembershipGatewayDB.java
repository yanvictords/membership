package com.team.membership.gateways.mysql;

import com.team.membership.gateways.MembershipGateway;
import com.team.membership.gateways.mysql.entities.MembershipEntity;
import com.team.membership.gateways.mysql.repositories.MembershipRepository;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class MembershipGatewayDB implements MembershipGateway {

  private final MembershipRepository membershipRepository;

  @Override
  public Optional<MembershipEntity> findByTeamAndUser(
      final String teamUuid, final String userUuid) {
    return membershipRepository.findByTeamUuidAndUserUuid(teamUuid, userUuid);
  }

  @Override
  public List<MembershipEntity> findByRoleId(final BigInteger roleId) {
    return membershipRepository.findAllByRoleId(roleId);
  }

  @Override
  public void save(final MembershipEntity membershipEntity) {
    membershipRepository.save(membershipEntity);
  }
}
