package com.team.membership.gateways.mysql;

import com.team.membership.gateways.RoleGateway;
import com.team.membership.gateways.mysql.entities.RoleEntity;
import com.team.membership.gateways.mysql.repositories.RoleRepository;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class RoleGatewayDB implements RoleGateway {

  private final RoleRepository roleRepository;

  @Override
  public List<RoleEntity> findAll() {
    return roleRepository.findAll();
  }

  @Override
  public Optional<RoleEntity> findByName(final String name) {
    return roleRepository.findByName(name);
  }

  @Override
  public void save(final RoleEntity role) {
    roleRepository.save(role);
  }
}
