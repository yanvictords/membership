package com.team.membership.gateways.mysql.converters.domain;

import com.team.membership.domains.Membership;
import com.team.membership.gateways.mysql.entities.MembershipEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MembershipConverter {

  public static Membership convert(final MembershipEntity membership) {
    final var roles = RoleConverter.convert(membership.getRole());
    return new Membership(membership.getTeamUuid(), membership.getUserUuid(), roles);
  }
}
