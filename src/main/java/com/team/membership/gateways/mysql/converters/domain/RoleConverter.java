package com.team.membership.gateways.mysql.converters.domain;

import com.team.membership.domains.Role;
import com.team.membership.gateways.mysql.entities.RoleEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RoleConverter {

  public static Role convert(final RoleEntity roleEntity) {
    return new Role(roleEntity.getId(), roleEntity.getName());
  }
}
