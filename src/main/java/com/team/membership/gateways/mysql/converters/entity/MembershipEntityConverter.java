package com.team.membership.gateways.mysql.converters.entity;

import com.team.membership.domains.Membership;
import com.team.membership.gateways.mysql.entities.MembershipEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MembershipEntityConverter {

  public static MembershipEntity convert(final Membership membership) {
    final var role = RoleEntityConverter.convert(membership.getRole());
    return new MembershipEntity(membership.getTeamUuid(), membership.getUserUuid(), role);
  }
}
