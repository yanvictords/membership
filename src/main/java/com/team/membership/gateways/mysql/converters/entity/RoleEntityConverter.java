package com.team.membership.gateways.mysql.converters.entity;

import com.team.membership.domains.Role;
import com.team.membership.gateways.mysql.entities.RoleEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RoleEntityConverter {

  public static RoleEntity convert(final Role role) {
    return new RoleEntity(role.getId(), role.getName());
  }
}
