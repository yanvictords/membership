package com.team.membership.gateways.mysql.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
@Table(name = "membership")
@AllArgsConstructor
@IdClass(MembershipKey.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MembershipEntity {

  @Id
  @Column(name = "TEAM_UUID")
  private String teamUuid;

  @Id
  @Column(name = "USER_UUID")
  private String userUuid;

  @JoinColumn(name = "ROLE_ID")
  @ManyToOne
  private RoleEntity role;
}
