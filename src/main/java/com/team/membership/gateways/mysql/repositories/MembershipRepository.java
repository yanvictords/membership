package com.team.membership.gateways.mysql.repositories;

import com.team.membership.gateways.mysql.entities.MembershipEntity;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MembershipRepository extends JpaRepository<MembershipEntity, Integer> {
  Optional<MembershipEntity> findByTeamUuidAndUserUuid(String teamUuid, String userUuid);

  List<MembershipEntity> findAllByRoleId(BigInteger roleId);
}
