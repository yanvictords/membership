package com.team.membership.usecases;

import static java.util.stream.Collectors.toList;

import com.team.membership.domains.Membership;
import com.team.membership.exceptions.ResourceNotFoundException;
import com.team.membership.gateways.MembershipGateway;
import com.team.membership.gateways.mysql.converters.domain.MembershipConverter;
import com.team.membership.gateways.mysql.converters.entity.MembershipEntityConverter;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MembershipUsecase {

  private final MembershipGateway membershipGateway;

  private final RoleUsecase roleUsecase;

  public Membership findMembership(final String teamUuid, final String userUuid) {
    return membershipGateway
        .findByTeamAndUser(teamUuid, userUuid)
        .map(MembershipConverter::convert)
        .orElseThrow(() -> new ResourceNotFoundException("Membership not found."));
  }

  public List<Membership> findMembershipByRole(final String roleName) {
    final var role = roleUsecase.findByName(roleName);
    return membershipGateway.findByRoleId(role.getId()).stream()
        .map(MembershipConverter::convert)
        .collect(toList());
  }

  public void save(final Membership membership, final String roleName) {
    final var role = roleUsecase.findByName(roleName);
    membership.setRole(role);
    membershipGateway.save(MembershipEntityConverter.convert(membership));
  }
}
