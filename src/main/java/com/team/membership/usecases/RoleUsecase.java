package com.team.membership.usecases;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

import com.team.membership.domains.Role;
import com.team.membership.exceptions.ResourceConflictException;
import com.team.membership.exceptions.ResourceNotFoundException;
import com.team.membership.gateways.RoleGateway;
import com.team.membership.gateways.mysql.converters.domain.RoleConverter;
import com.team.membership.gateways.mysql.entities.RoleEntity;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoleUsecase {

  private final RoleGateway roleGateway;

  public List<Role> findAll() {
    return roleGateway.findAll().stream().map(RoleConverter::convert).collect(toList());
  }

  public Role findByName(String name) {
    return roleGateway
        .findByName(name)
        .map(RoleConverter::convert)
        .orElseThrow(
            () -> new ResourceNotFoundException(format("Role %s is not registered.", name)));
  }

  public void save(final String roleName) {
    roleGateway
        .findByName(roleName)
        .ifPresent(
            r -> {
              throw new ResourceConflictException(
                  format("Role %s is already registered.", roleName));
            });

    roleGateway.save(new RoleEntity(roleName));
  }
}
