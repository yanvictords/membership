package com.team.membership.controllers;

import static java.math.BigInteger.ONE;
import static org.assertj.core.util.Lists.newArrayList;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.team.membership.controllers.contracts.RoleContract;
import com.team.membership.domains.Membership;
import com.team.membership.domains.Role;
import com.team.membership.usecases.MembershipUsecase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(MembershipController.class)
public class MembershipControllerTest {
  @MockBean MembershipUsecase membershipUsecase;

  @Autowired private MockMvc mockMvc;

  @Test
  void get_by_team_and_user() {
    final var teamUuid = "teamUuid";
    final var userUuid = "userUuid";

    final var mock = new Membership(teamUuid, userUuid, new Role(ONE, "role"));
    when(membershipUsecase.findMembership(teamUuid, userUuid)).thenReturn(mock);

    Assertions.assertDoesNotThrow(
        () ->
            mockMvc
                .perform(get("/teams/{team_uuid}/users/{user_uuid}", teamUuid, userUuid))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty()));
  }

  @Test
  void get_by_role_name() {
    final var roleName = "role";
    final var teamUuid = "teamUuid";
    final var userUuid = "userUuid";

    final var mock = new Membership(teamUuid, userUuid, new Role(ONE, "roleName"));
    when(membershipUsecase.findMembershipByRole(roleName)).thenReturn(newArrayList(mock));

    Assertions.assertDoesNotThrow(
        () ->
            mockMvc
                .perform(get("/teams/users/roles/{role_name}", roleName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty()));
  }

  @Test
  void save_membership() {
    final var roleName = "role";
    final var teamUuid = "teamUuid";
    final var userUuid = "userUuid";

    final var mock = new RoleContract(roleName);

    Assertions.assertDoesNotThrow(
        () ->
            mockMvc
                .perform(
                    post("/teams/{team_uuid}/users/{user_uuid}", teamUuid, userUuid)
                        .contentType(APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(mock)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$").isNotEmpty()));
  }
}
