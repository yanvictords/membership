package com.team.membership.controllers;

import static java.math.BigInteger.ONE;
import static org.assertj.core.util.Lists.newArrayList;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.team.membership.controllers.contracts.RoleContract;
import com.team.membership.domains.Role;
import com.team.membership.usecases.RoleUsecase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(RoleController.class)
public class RoleControllerTest {
  @MockBean RoleUsecase roleUsecase;

  @Autowired private MockMvc mockMvc;

  @Test
  void get_all() {

    when(roleUsecase.findAll()).thenReturn(newArrayList(new Role(ONE, "role")));
    Assertions.assertDoesNotThrow(
        () ->
            mockMvc
                .perform(get("/roles"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty()));
  }

  @Test
  void save_role() {
    final var roleName = "role";
    final var mock = new RoleContract(roleName);

    Assertions.assertDoesNotThrow(
        () ->
            mockMvc
                .perform(
                    post("/roles")
                        .contentType(APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(mock)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$").isNotEmpty()));
  }
}
