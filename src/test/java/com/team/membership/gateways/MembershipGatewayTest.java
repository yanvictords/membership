package com.team.membership.gateways;

import static java.math.BigInteger.ONE;
import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.team.membership.gateways.mysql.MembershipGatewayDB;
import com.team.membership.gateways.mysql.entities.MembershipEntity;
import com.team.membership.gateways.mysql.entities.RoleEntity;
import com.team.membership.gateways.mysql.repositories.MembershipRepository;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;

@MockitoSettings
public class MembershipGatewayTest {

  @Mock private MembershipRepository membershipRepository;

  @InjectMocks private MembershipGatewayDB membershipGateway;

  @Test
  void find_by_team_and_user() {
    final var teamUuid = "teamUuid";
    final var userUuid = "userUuid";
    final var mock = new MembershipEntity(teamUuid, userUuid, new RoleEntity(ONE, "role"));
    when(membershipRepository.findByTeamUuidAndUserUuid(eq(teamUuid), eq(userUuid)))
        .thenReturn(Optional.of(mock));

    final var response = membershipGateway.findByTeamAndUser(teamUuid, userUuid).get();

    assertEquals(mock.getTeamUuid(), response.getTeamUuid());
    assertEquals(mock.getUserUuid(), response.getUserUuid());
    assertEquals(mock.getRole().getName(), response.getRole().getName());
  }

  @Test
  void find_by_role_id() {
    final var roleId = ONE;
    final var teamUuid = "teamUuid";
    final var userUuid = "userUuid";
    final var mock = new MembershipEntity(teamUuid, userUuid, new RoleEntity(roleId, "role"));
    when(membershipRepository.findAllByRoleId(eq(roleId))).thenReturn(newArrayList(mock));

    final var response = membershipRepository.findAllByRoleId(roleId).get(0);

    assertEquals(mock.getTeamUuid(), response.getTeamUuid());
    assertEquals(mock.getUserUuid(), response.getUserUuid());
    assertEquals(mock.getRole().getName(), response.getRole().getName());
  }

  @Test
  void save_membership() {
    final var teamUuid = "teamUuid";
    final var userUuid = "userUuid";
    final var mock = new MembershipEntity(teamUuid, userUuid, new RoleEntity(ONE, "role"));
    membershipRepository.save(mock);
    verify(membershipRepository).save(any(MembershipEntity.class));
  }
}
