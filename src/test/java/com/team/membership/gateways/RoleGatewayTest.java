package com.team.membership.gateways;

import static java.math.BigInteger.ONE;
import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.team.membership.gateways.mysql.RoleGatewayDB;
import com.team.membership.gateways.mysql.entities.RoleEntity;
import com.team.membership.gateways.mysql.repositories.RoleRepository;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;

@MockitoSettings
public class RoleGatewayTest {

  @Mock private RoleRepository roleRepository;

  @InjectMocks private RoleGatewayDB roleGatewayDB;

  @Test
  void find_all() {
    final var mock = new RoleEntity(ONE, "role");
    when(roleGatewayDB.findAll()).thenReturn(newArrayList(mock));

    final var response = roleGatewayDB.findAll().get(0);

    assertEquals(mock.getId(), response.getId());
    assertEquals(mock.getName(), response.getName());
  }

  @Test
  void find_by_name() {
    final var roleName = "role";
    final var mock = new RoleEntity(ONE, roleName);
    when(roleGatewayDB.findByName(eq(roleName))).thenReturn(Optional.of(mock));

    final var response = roleGatewayDB.findByName(roleName).get();

    assertEquals(mock.getId(), response.getId());
    assertEquals(mock.getName(), response.getName());
  }

  @Test
  void save_role() {
    final var roleName = "role";
    final var mock = new RoleEntity(ONE, roleName);
    roleRepository.save(mock);
    verify(roleRepository).save(any(RoleEntity.class));
  }
}
