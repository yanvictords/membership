package com.team.membership.usecases;

import static java.math.BigInteger.ONE;
import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.team.membership.domains.Membership;
import com.team.membership.domains.Role;
import com.team.membership.exceptions.ResourceNotFoundException;
import com.team.membership.gateways.MembershipGateway;
import com.team.membership.gateways.mysql.entities.MembershipEntity;
import com.team.membership.gateways.mysql.entities.RoleEntity;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;

@MockitoSettings
public class MembershipUsecaseTest {

  @Mock private MembershipGateway membershipGateway;

  @Mock private RoleUsecase roleUsecase;

  @InjectMocks private MembershipUsecase membershipUsecase;

  @Test
  void find_membership_by_team_and_user() {
    final var teamUuid = "teamUuid";
    final var userUuid = "userUuid";

    final var mock = new MembershipEntity(teamUuid, userUuid, new RoleEntity(ONE, "role"));
    when(membershipGateway.findByTeamAndUser(eq(teamUuid), eq(userUuid)))
        .thenReturn(Optional.of(mock));

    final var response = membershipUsecase.findMembership(teamUuid, userUuid);

    assertEquals(mock.getTeamUuid(), response.getTeamUuid());
    assertEquals(mock.getUserUuid(), response.getUserUuid());
    assertEquals(mock.getRole().getName(), response.getRoleName());
  }

  @Test
  void find_membership_by_team_and_user_handle_not_found() {
    final var teamUuid = "teamUuid";
    final var userUuid = "userUuid";

    when(membershipGateway.findByTeamAndUser(eq(teamUuid), eq(userUuid)))
        .thenReturn(Optional.empty());

    assertThrows(
        ResourceNotFoundException.class,
        () -> membershipUsecase.findMembership(teamUuid, userUuid));
  }

  @Test
  void find_membership_by_role() {
    final var role = "role";

    final var mockRole = new Role(ONE, role);
    when(roleUsecase.findByName(eq(mockRole.getName()))).thenReturn(mockRole);

    final var mock = new MembershipEntity("teamUuid", "userUuid", new RoleEntity(ONE, role));
    when(membershipGateway.findByRoleId(eq(mockRole.getId()))).thenReturn(newArrayList(mock));

    final var response = membershipUsecase.findMembershipByRole(role).get(0);

    assertEquals(mock.getTeamUuid(), response.getTeamUuid());
    assertEquals(mock.getUserUuid(), response.getUserUuid());
    assertEquals(mock.getRole().getName(), response.getRoleName());
  }

  @Test
  void create_membership() {
    final var role = "role";
    final var mockRole = new Role(ONE, role);
    when(roleUsecase.findByName(eq(mockRole.getName()))).thenReturn(mockRole);

    final var membership = new Membership("teamUuid", "userUuid", new Role(ONE, role));

    membershipUsecase.save(membership, role);

    verify(membershipGateway).save(any(MembershipEntity.class));
  }
}
