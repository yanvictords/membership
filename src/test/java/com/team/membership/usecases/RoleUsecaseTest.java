package com.team.membership.usecases;

import static java.math.BigInteger.ONE;
import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.team.membership.exceptions.ResourceConflictException;
import com.team.membership.exceptions.ResourceNotFoundException;
import com.team.membership.gateways.RoleGateway;
import com.team.membership.gateways.mysql.entities.RoleEntity;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;

@MockitoSettings
public class RoleUsecaseTest {

  @Mock private RoleGateway roleGateway;

  @InjectMocks private RoleUsecase roleUsecase;

  @Test
  void find_all() {
    final var mock = new RoleEntity(ONE, "role");
    when(roleGateway.findAll()).thenReturn(newArrayList(mock));

    final var response = roleUsecase.findAll().get(0);

    assertEquals(mock.getId(), response.getId());
    assertEquals(mock.getName(), response.getName());
  }

  @Test
  void find_by_name() {
    final var name = "role";
    final var mock = new RoleEntity(ONE, name);
    when(roleGateway.findByName(eq(name))).thenReturn(Optional.of(mock));

    final var response = roleUsecase.findByName(name);

    assertEquals(mock.getId(), response.getId());
    assertEquals(mock.getName(), response.getName());
  }

  @Test
  void find_by_name_handle_not_found() {
    final var name = "role";
    when(roleGateway.findByName(eq(name))).thenReturn(Optional.empty());
    assertThrows(ResourceNotFoundException.class, () -> roleUsecase.findByName(name));
  }

  @Test
  void save_role() {
    roleUsecase.save("role");
    verify(roleGateway).save(any(RoleEntity.class));
  }

  @Test
  void save_role_handle_conflict() {
    final var name = "role";
    when(roleGateway.findByName(eq(name))).thenReturn(Optional.of(new RoleEntity(ONE, name)));
    assertThrows(ResourceConflictException.class, () -> roleUsecase.save(name));
  }
}
